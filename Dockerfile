FROM alpine:3.14

RUN apk add --no-cache mariadb mariadb-client

COPY scripts /usr/local/bin

CMD [ "--user=mysql" ]

ENTRYPOINT [ "entrypoint.sh" ]
